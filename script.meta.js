// ==UserScript==
// @name         QQ: Available Format Checker
// @version      1.1.0
// @match        https://y.qq.com/n/yqq/album/*
// @match        https://y.qq.com/n/yqq/singer/*
// @match        https://y.qq.com/n/ryqq/albumDetail/*
// @match        https://y.qq.com/n/ryqq/singer/*
// @grant        GM.xmlHttpRequest
// @updateURL    https://gitlab.com/Iksxsuzil/qq-available-format-checker/-/raw/master/script.meta.js
// @downloadURL  https://gitlab.com/Iksxsuzil/qq-available-format-checker/-/raw/master/script.user.js
// @homepageURL  https://gitlab.com/Iksxsuzil/qq-available-format-checker/
// @supportURL   https://gitlab.com/Iksxsuzil/qq-available-format-checker/issues
// ==/UserScript==
