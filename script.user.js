/* globals GM */
// ==UserScript==
// @name         QQ: Available Format Checker
// @version      1.1.0
// @match        https://y.qq.com/n/yqq/album/*
// @match        https://y.qq.com/n/yqq/singer/*
// @match        https://y.qq.com/n/ryqq/albumDetail/*
// @match        https://y.qq.com/n/ryqq/singer/*
// @grant        GM.xmlHttpRequest
// @updateURL    https://gitlab.com/Iksxsuzil/qq-available-format-checker/-/raw/master/script.meta.js
// @downloadURL  https://gitlab.com/Iksxsuzil/qq-available-format-checker/-/raw/master/script.user.js
// @homepageURL  https://gitlab.com/Iksxsuzil/qq-available-format-checker/
// @supportURL   https://gitlab.com/Iksxsuzil/qq-available-format-checker/issues
// ==/UserScript==

'use strict';

document.head.insertAdjacentHTML('beforeend', '<style>.songlist__header_mp3,.songlist__size_mp3{position:absolute;top:0;left:505px}' +
								'.songlist__header_flac,.songlist__size_flac{position:absolute;top:0;left:425px}' +
								'.songlist__size_flac{font-size:14px;line-height:50px}' +
								'.songlist__size_mp3{font-size:14px;line-height:50px}</style>');

const header = `<li class="songlist__header_mp3">MP3 320</li>
      <li class="songlist__header_flac">FLAC</li>`;

const bytesToSize = (bytes, decimals = 2) => {
  if (bytes === 0) return 'x';
  const k = 1000;
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  const size = bytes / Math.pow(k, i);
  const unit = [' Bytes', ' KB', ' MB', ' GB'][i];
  return size.toFixed(decimals) + unit;
};

const secToHMS = (seconds) => {
  const date = new Date(seconds * 1000).toISOString();
  const start = seconds < 3600 ? 14 : 11;
  const end = date.length - 5;
  return date.substring(start, end);
};

const templateArtists = (artists) => 
  artists.map(({ mid, name }) => {
		return `<a class="playlist__author" title="${name}" href="/n/ryqq/singer/${mid}">${name}</a>`;
	}).join(' / ');

const templateSize = ({ size_flac = 0, size_320mp3 = 0 }) => 
`<div class="songlist__size_flac">${bytesToSize(size_flac)}</div>
<div class="songlist__size_mp3">${bytesToSize(size_320mp3)}</div>`;

const templateTrack = (number, { id, mid, title, singer, interval, file }) => 
`<li>
  <div class="songlist__item songlist__item--even">
    <div class="songlist__number songlist__number--top">${number}</div>
    <div class="songlist__songname">
      <span class="songlist__songname_txt">
        <a title="${title}" href="/n/ryqq/songDetail/${mid}">${title}</a>
      </span>
    </div>
    <div class="songlist__artist">${templateArtists(singer)}</div>
    <div class="songlist__time">${secToHMS(interval)}</div>
    ${templateSize(file)}
</li>`;

const templateArtistRelease = ({ albumMid, albumName, publishDate }) => 
`<li class="playlist__item">
  <div class="playlist__item_box">
    <div class="playlist__cover mod_cover">
      <a class="js_album" href="/n/ryqq/albumDetail/${albumMid}">
        <img class="playlist__pic" loading="lazy"
             src="//y.qq.com/music/photo_new/T002R300x300M000${albumMid}.jpg?max_age=2592000"
             alt="${albumName}"
             data-qar-def="//y.qq.com/mediastyle/global/img/album_300.png?max_age=2592000">
        <i class="mod_cover__icon_play js_play"></i>
      </a>
    </div>
    <h4 class="playlist__title">
      <span class="playlist__title_txt">
        <a title="${albumName}"
           class="js_album"
           href="/n/ryqq/albumDetail/${albumMid}">${albumName}</a>
      </span>
    </h4>
    <div class="playlist__other">${publishDate}</div>
  </div>
</li>`;

const setArtistRelease = (albumList) => {
	const releases = document.querySelector('.playlist__list.mod_playlist');
	for (const release of albumList) {
		const { albumMid } = release;
		const element = document.querySelector(`[href="/n/ryqq/albumDetail/${albumMid}"]`);
		if (element) continue;
		const html = templateArtistRelease(release);
		releases.insertAdjacentHTML('beforeEnd', html);
	}
};

const setTracklist = (songList) => {
	const tracklist = document.querySelector('.songlist__list');
  for (let i = 0; i < songList.length; ++i) {
    const { songInfo } = songList[i];
    const { file, mid } = songInfo;
		const element = document.querySelector(`[href="/n/ryqq/songDetail/${mid}"]`);
		if (element) {
			const { parentNode } = element.parentNode.parentNode;
			const sizes = templateSize(file);
			parentNode.insertAdjacentHTML('beforeEnd', sizes);
		} else {
			const html = templateTrack(++i, songInfo);
			tracklist.insertAdjacentHTML('beforeEnd', html);
		}
	}
};

const types = {
  a: {
    method: 'GetAlbumSongList',
    qmodule: 'AlbumSongList',
    methodKey: 'albumSonglist',
    midType: 'albumMid',
    idType: 'albumID',
    num: -1,
    cv: 10000,
  },
  s: {
    method: 'GetAlbumList',
    qmodule: 'AlbumListServer',
    methodKey: 'getAlbumList',
    midType: 'singerMid',
    idType: 'singerID',
    num: 100,
    cv: 0,
  },
};

const start = (begin = 0) => {

  const [, type, id] = /\/(\w+)\/(\w+)(\.html)?$/.exec(location.pathname);

  const char = type.charAt();
  const {
    method,
    qmodule,
    methodKey,
    midType,
    idType,
    num,
    cv
  } = types[char];

	const callback =  methodKey + String(Math.random()).replace('0.', '');

	const params = {
		'-': callback,
		g_tk: 5381,
		loginUin: 0,
		hostUin: 0,
		format: 'json',
		inCharset: 'utf8',
		outCharset: 'utf-8',
		notice: 0,
		platform: 'yqq.json',
		needNewCode: 0,
		data: {
			comm: {
				ct: 24,
				cv: cv,
			},
			[methodKey]: {
				method,
				param: {
					[midType]: id,
					[idType]: 0,
					num: num,
					begin: begin,
					order: 2
				},
				module: `music.musichallAlbum.${qmodule}`
			}
		}
	};

	const url = new URL('https://u.y.qq.com/cgi-bin/musicu.fcg');

	for (const [key, value] of Object.entries(params)) {
		url.searchParams.set(key, JSON.stringify(value));
	}

	GM.xmlHttpRequest({
		url: url.toString(),
		method: 'GET',
		responseType: 'json',
		onload: e => {

			if (e.status === 200) {

				const { albumSonglist, getAlbumList } = e.response;
				const { data: { albumList, total = 0, songList }} = albumSonglist || getAlbumList;
				if (type.startsWith('album')) {
          document.querySelector('.songlist__header')
            .insertAdjacentHTML('beforeEnd', header);
          setTracklist(songList);
        } else {
          if (begin > 0) {
            setArtistRelease(albumList);
          } else {
            setTimeout(setArtistRelease.bind(undefined, albumList), 0);
          }
        } 

				if (begin < total) return start(begin + 80);

			}

		},
		onerror: console.error
	});

};

start();
